# Contribuer

## Environnements et branches

Ce dépôt Git possède 2 branches qui correspondent chacun à un environnement :

* **master** : environnement de prod (<https://lequartier.animafac.net/>),
  correspond à `/srv/www/plateforme.animafac.net/public_html` sur le serveur
* **develop** : environnement de dev (<https://dev.lequartier.animafac.net/>),
  correspond à `/srv/www/plateforme.animafac.net/dev/` sur le serveur

### Déployer une modification en dev

#### En travaillant directement sur l'environnement de dev

* Faire sa modification en SSH sur le serveur
* Committer sa modification sur la branche develop et pusher

#### En faisant tourner le projet localement

* Faire sa modification en local
* Committer sa modification sur la branche develop et pusher
* En SSH sur le serveur, aller dans le dossier de dev
  et lancer `git pull && composer install --no-dev -o`
* En cas de mise à jour du thème, aller dans le dossier du thème
  et lancer `yarn --prod && yarn build-production`

### Déployer une modification en prod

* Merger la branche develop dans la branche master
  (en vérifiant bien que tout ce qu'on merge peut être déployé en prod)
  et pusher
* En SSH sur le serveur, aller dans le dossier de prod
  et lancer `git pull && composer install --no-dev -o`
* En cas de mise à jour du thème, aller dans le dossier du thème
  et lancer `yarn --prod && yarn build-production`

## Modifier le thème

Le thème n'est pas directement dans ce dépôt,
mais dans [un dépôt Git séparé](https://framagit.org/Animafac/wordpress/wp-theme-plateforme/).
Il est installé avec Composer.

Pour modifier le thème, il faut :

* Faire un commit sur la branche master du thème et le pusher.
* Lancer `composer update animafac/wp-theme-plateforme`
  pour que ce dernier commit soit référencé dans `composer.lock`
  (et donc installé quand on lance `composer intall`).
* Faire un commit sur ce projet le pusher.

Pour résumer, si on veut déployer une modification du thème, il faut :

* Sur la branche develop du dépôt civicrm-animafac, faire un composer update animafac/wp-theme-plateforme pour mettre le thème à jour, puis committer la modification du fichier composer.lock.
* Fusionner develop dans master.
* Puller master sur la prod et faire un composer install + compiler le thème.