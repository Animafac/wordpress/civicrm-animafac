# CiviCRM Animafac setup

This repository contains a [Bedrock](https://roots.io/bedrock/) setup
used to manage the WordPress/CiviCRM instance on [lequartier.animafac.net](https://lequartier.animafac.net/).

## Setup

First, you need to create your config file:

```bash
cp .env.example .env
```

And add your database credentials and website URL to it:

```env
DB_NAME=database_name
DB_USER=database_user
DB_PASSWORD=database_password

WP_HOME=http://url/to/web
```

Then install the dependencies with [Composer](https://getcomposer.org/):

```bash
composer install
```

(Some dependencies are pulled from private repositories
so you will need access to them.)

Browse to `wp-admin/` in order to finish the installation.

## Plugins

The following WordPress plugins are used:

* **Advanced Custom Fields**:
    Adds custom fields used by the theme in the admin.
* **bbPress**:
    Used to create the forums ("Petites annonces").
* **bbPress Mentions Email Notifications**:
    Adds support for @mentions in the forums.
* **bbPress Messages**:
    Adds support for direct messages.
* **Buddy-bbPress Support Topic**:
    Allows to have support topics (that can be marked as resolved).
* **CiviCRM**:
    Adds the CiviCRM interface in the admin, allowing us to manage contacts.
* **CiviCRM Front Office**:
    Custom plugin that adds shortcodes used to
    display info from the CiviCRM API in the theme.
* **CiviCRM WordPress Profile Sync**:
    Keep the names of WordPress users and CiviCRM contacts in sync.
* **SecuPress Free**:
    Check if WordPress settings are secure enough.
* **Timber**:
    Allows us to use Twig templates in the theme.
* **WP User Avatar**:
    Allows user to upload their own avatar.
* **WP-Piwik**:
    Adds a Piwik tracking code in every page.

Most plugins are heavily used by the theme
so disabling them would probably require some changes to the templates.

## Composer tasks

Composer can run [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)
to check if the PHP code is formatted correctly:

```bash
composer lint
```

## JavaScript tools

Some useful JavaScript tools can be installed with [Yarn](https://yarnpkg.com/):

```bash
yarn install
```

[fixpack](https://github.com/henrikjoreteg/fixpack)
can be used to make sure `package.json` is valid:

```bash
yarn fixpack
```

[markdownlint](https://github.com/DavidAnson/markdownlint)
can be used to make sure `README.md` is formatted correctly:

```bash
yarn markdownlint *.md
```
