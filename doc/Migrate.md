# Migrate to another server

First, clone the setup on the new server:

```bash
git clone https://framagit.org/Animafac/civicrm-animafac.git path/to/webroot
```

Then, export the database from the original server and import it on the new server.

Now, create your config file:

```bash
cp .env.example .env
```

And add your database credentials and website URL to it:

```env
DB_NAME=database_name
DB_USER=database_user
DB_PASSWORD=database_password

WP_HOME=http://url/to/web
```

Then install the dependencies with [Composer](https://getcomposer.org/):

```bash
composer install
```

(Some dependencies are pulled from private repositories
so you will need access to them.)

You also need to copy the `web/app/uploads/civicrm/civicrm.settings.php` file
from the original server and add your database credentials in it
(in two different places):

```php
if (!defined('CIVICRM_UF_DSN') && CIVICRM_UF !== 'UnitTests') {
  define( 'CIVICRM_UF_DSN'           , 'mysql://database_user:database_password@localhost:3306/database_name?new_link=true');
}
```

```php
if (!defined('CIVICRM_DSN')) {
  if (CIVICRM_UF === 'UnitTests' && isset($GLOBALS['_CV']['TEST_DB_DSN'])) {
    define('CIVICRM_DSN', $GLOBALS['_CV']['TEST_DB_DSN']);
  }
  else {
    define('CIVICRM_DSN', 'mysql://database_user:database_password@localhost:3306/database_name?new_link=true');
  }
}
```

You also need to fix some paths:

```php
$civicrm_root = '/path/to/web/app/plugins/civicrm/civicrm/';
if (!defined('CIVICRM_TEMPLATE_COMPILEDIR')) {
  define( 'CIVICRM_TEMPLATE_COMPILEDIR', '/path/to/web/app/uploads/civicrm/templates_c/');
}
```
