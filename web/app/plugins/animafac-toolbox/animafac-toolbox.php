<?php

/*
Plugin Name: Animafac Toolbox
Author: Insite
Author URI: https://www.insite.coop/
*/

use AnimafacToolbox\Migrate;
use AnimafacToolbox\Toolbox;

add_action( 'init', array( Toolbox::class, 'init' ) );
add_action( 'admin_menu', array( Toolbox::class, 'adminMenu' ) );
add_action( 'wp_head', array( Toolbox::class, 'wpHead' ) );
add_action( 'widgets_init', array( Toolbox::class, 'widgets_init' ) );
add_filter( 'parent_file', array( Toolbox::class, 'parent_file' ) );

add_action( 'import_post_meta', array( Migrate::class, 'importPostMeta' ), 10, 3 );
add_filter( 'import_allow_create_users', '__return_false' );
add_filter( 'image_sideload_extensions', array( Migrate::class, 'imageSideloadExtensions' ) );
add_filter( 'wp_import_post_data_processed', array( Migrate::class, 'wpImportPostDataProcessed' ) );
