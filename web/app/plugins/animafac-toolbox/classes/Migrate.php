<?php

namespace AnimafacToolbox;

use DOMDocument;
use DOMElement;
use DOMXPath;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Vnn\WpApiClient\Http\GuzzleAdapter;
use Vnn\WpApiClient\WpClient;
use WP_Error;

class Migrate {


	/**
	 * @param string[] $allowed_extensions
	 *
	 * @return string[]
	 */
	public static function imageSideloadExtensions( array $allowed_extensions ): array {
		$allowed_extensions[] = 'zip';
		$allowed_extensions[] = 'pdf';
		$allowed_extensions[] = 'xlsx';
		$allowed_extensions[] = 'xls';
		$allowed_extensions[] = 'doc';

		return $allowed_extensions;
	}

	/**
	 * @param int $id
	 * @param int $post_id
	 *
	 * @return int|string|\WP_Error|null
	 */
	private static function fetchMedia( int $id, int $post_id ) {
		try {
			$client = new WpClient( new GuzzleAdapter( new Client() ), 'https://www.animafac.net/' );
			$media  = $client->media()->get( $id );

			if ( $existingId = post_exists( $media['title']['rendered'], null, null, $media['type'] ) ) {
				// Le média a déjà été importé.
				return $existingId;
			}

			$result = media_sideload_image( $media['source_url'], $post_id, $media['title']['rendered'], 'id' );

			if ( $result instanceof WP_Error ) {
				error_log( "Impossible d'importer le média " . $id . ' : ' . $result->get_error_message() );
			}

			return $result;
		} catch ( ClientException $e ) {
			// On a des 401 sur certains médias parce qu'ils sont rattachés à un contenu privé.
			error_log( "Impossible d'importer le média " . $id );

			return null;
		}
	}

	/**
	 * @param int          $post_id
	 * @param string       $key
	 * @param string|array $value
	 *
	 * @return void
	 */
	public static function importPostMeta( int $post_id, string $key, $value ) {
		for ( $i = 0; $i <= 5; $i ++ ) {
			if ( $key == 'fiche_attachment_' . $i . '_fiche_attachment_file' && ! empty( $value ) ) {
				$result = self::fetchMedia( $value, $post_id );

				if ( ! $result instanceof WP_Error && ! is_null( $result ) ) {
					update_post_meta( $post_id, $key, $result );
				}
			}
		}
		if ( in_array( $key, array( 'content_thumbnail', 'content_file' ) ) && ! empty( $value ) ) {
			$result = self::fetchMedia( $value, $post_id );

			if ( ! $result instanceof WP_Error && ! is_null( $result ) ) {
				update_post_meta( $post_id, $key, $result );
			}
		} elseif ( $key == 'content_gallery' && is_array( $value ) ) {
			$medias = $value;
			foreach ( $value as $i => $id ) {
				$result = self::fetchMedia( $id, $post_id );

				if ( ! $result instanceof WP_Error && ! is_null( $result ) ) {
					$medias[ $i ] = $result;
				}
			}

			update_post_meta( $post_id, $key, $medias );
		}
	}

	/**
	 * @param string $html
	 *
	 * @return \DOMDocument
	 * @link https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Component%21Utility%21Html.php/function/Html%3A%3Aload/9.3.x
	 * @noinspection HtmlRequiredLangAttribute
	 * @noinspection HtmlRequiredTitleElement
	 */
	public static function loadHtml( string $html ): DOMDocument {
		$document = <<<EOD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>!html</body>
</html>
EOD;

		$document = strtr(
			$document,
			array(
				"\n"    => '',
				'!html' => $html,
			)
		);
		$dom      = new DOMDocument();

		@$dom->loadHTML( $document );

		return $dom;
	}

	/**
	 * @param \DOMDocument $document
	 *
	 * @return string
	 * @link https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Component%21Utility%21Html.php/function/Html%3A%3Aserialize/9.3.x
	 */
	public static function serializeHtml( DOMDocument $document ): string {
		$body_node = $document
			->getElementsByTagName( 'body' )
			->item( 0 );
		$html      = '';
		if ( $body_node !== null ) {
			foreach ( $body_node->childNodes as $node ) {
				$html .= $document->saveXML( $node );
			}
		}

		return $html;
	}

	/**
	 * @param \DOMElement $node
	 * @param string      $attribute
	 * @param int         $post_id
	 *
	 * @return void
	 */
	private static function replaceUrl( DOMElement $node, string $attribute, int $post_id ) {
		$client   = new WpClient( new GuzzleAdapter( new Client() ), 'https://www.animafac.net/' );
		$url      = $node->getAttribute( $attribute );
		$path     = parse_url( $url, PHP_URL_PATH );
		$filename = basename( $path );

		if ( str_starts_with( $path, '/media/' ) ) {
			$list = $client->media()->get( null, array( 'search' => $filename ) );
			if ( $mediaInfo = current( $list ) ) {
				$result = self::fetchMedia( $mediaInfo['id'], $post_id );

				if ( ! $result instanceof WP_Error && ! is_null( $result ) ) {
					$node->setAttribute( $attribute, wp_get_attachment_url( $result ) );
				}
			}
		} elseif ( str_starts_with( $path, '/guides-pratiques/' )
			|| str_starts_with( $path, '/kits-de-campagne/' )
			|| str_starts_with( $path, '/model/' )
			|| str_starts_with( $path, '/fiches-pratiques/' )
			|| str_starts_with( $path, '/video/' )
		) {
			// On remplace le domaine.
			$node->setAttribute(
				$attribute,
				str_replace(
					parse_url( $url, PHP_URL_HOST ),
					parse_url( get_site_url(), PHP_URL_HOST ),
					$url
				)
			);
		}
	}

	/**
	 * @param array $postdata
	 *
	 * @return array
	 */
	public static function wpImportPostDataProcessed( array $postdata ): array {
		$dom   = self::loadHtml( $postdata['post_content'] );
		$xpath = new DOMXPath( $dom );

		/** @var \DOMElement $node */
		foreach ( $xpath->query(
			'//a[starts-with(@href, "/") or contains(@href, "animafac.net/")]'
		) as $node ) {
			self::replaceUrl( $node, 'href', $postdata['import_id'] );
		}

		/** @var \DOMElement $node */
		foreach ( $xpath->query(
			'//img[starts-with(@src, "/") or contains(@src, "animafac.net/")]'
		) as $node ) {
			self::replaceUrl( $node, 'src', $postdata['import_id'] );
		}

		$postdata['post_content'] = self::serializeHtml( $dom );

		return $postdata;
	}
}
