<?php

namespace AnimafacToolbox;

use AnimafacToolbox\Widgets\About;
use AnimafacToolbox\Widgets\Files;
use AnimafacToolbox\Widgets\License;
use AnimafacToolbox\Widgets\Related;
use AnimafacToolbox\Widgets\TOC;
use TOC\MarkupFixer;

class Toolbox {


	/**
	 * @return void
	 */
	private static function addWiki() {
		register_post_type(
			'incsub_wiki',
			array(
				'labels'             => array(
					'name'               => __( 'Fiches pratiques', 'wiki' ),
					'singular_name'      => __( 'Fiche pratique', 'wiki' ),
					'add_new'            => __( 'Ajouter une fiche', 'wiki' ),
					'add_new_item'       => __( 'Ajouter une fiche', 'wiki' ),
					'edit_item'          => __( 'Modifier une fiche', 'wiki' ),
					'new_item'           => __( 'Ajouter une fiche', 'wiki' ),
					'view_item'          => __( 'Voir la fiche', 'wiki' ),
					'search_items'       => __( 'Chercher une fiche', 'wiki' ),
					'not_found'          => __( 'Aucun résultat', 'wiki' ),
					'not_found_in_trash' => __( 'Aucun résultat dans la corbeille', 'wiki' ),
					'menu_name'          => __( 'Fiches pratiques', 'wiki' ),
				),
				'public'             => true,
				'hierarchical'       => true,
				'map_meta_cap'       => true,
				'query_var'          => true,
				'supports'           => array(
					'title',
					'editor',
					'author',
					'revisions',
					'comments',
					'page-attributes',
					'thumbnail',
				),
				'has_archive'        => true,
				'publicly_queryable' => true,
				'rewrite'            => array(
					'slug'       => 'fiches-pratiques',
					'with_front' => false,
				),
				'taxonomies'         => array(
					'themes',
					'actions',
				),
				'show_in_menu'       => 'animafac-tools',
				'show_in_rest'       => true,
			)
		);

		$fieldGroup = array(
			'id'         => 'acf_ficheguide-pratique',
			'title'      => 'Fiche/guide pratique',
			'fields'     => array(
				array(
					'key'          => 'field_532307c019210',
					'label'        => 'Illustration principale',
					'name'         => 'content_thumbnail',
					'type'         => 'image',
					'save_format'  => 'id',
					'preview_size' => 'thumbnail',
					'library'      => 'uploadedTo',
				),
				array(
					'key'             => 'field_5328a6a6173bb',
					'label'           => 'Association de contenus',
					'name'            => 'content_featured',
					'type'            => 'relationship',
					'return_format'   => 'object',
					'post_type'       => array(
						0 => 'page',
						1 => 'blog',
						2 => 'kit',
						3 => 'anim',
						4 => 'guide',
						5 => 'event',
					),
					'taxonomy'        => array(
						0 => 'all',
					),
					'filters'         => array(
						0 => 'search',
					),
					'result_elements' => array(
						0 => 'post_type',
						1 => 'post_title',
					),
					'max'             => 4,
				),
				array(
					'key'   => 'files_accordion',
					'label' => 'Fichiers joints',
					'name'  => 'files_accordion',
					'type'  => 'accordion',
				),
			),
			'location'   => array(
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'incsub_wiki',
						'order_no' => 0,
						'group_no' => 0,
					),
				),
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'guide',
						'order_no' => 0,
						'group_no' => 1,
					),
				),
			),
			'options'    => array(
				'position'       => 'normal',
				'layout'         => 'no_box',
				'hide_on_screen' => array(
					0 => 'excerpt',
					1 => 'custom_fields',
					2 => 'discussion',
					3 => 'revisions',
					4 => 'slug',
					5 => 'author',
					6 => 'format',
					7 => 'featured_image',
					8 => 'tags',
					9 => 'send-trackbacks',
				),
			),
			'menu_order' => 0,
		);

		/*
		 * Sur l'ancien site, c'était un champ repeater
		 * mais ce type de champ n'est disponible que dans ACF Pro,
		 * donc à la place on utilise plusieurs groupes structurés pareil.
		 * Aucun fiche de l'ancien site n'a plus de 5 fichiers.
		 */
		for ( $i = 0; $i <= 5; $i ++ ) {
			$fieldGroup['fields'][] = array(
				'key'          => 'fiche_attachment_' . $i,
				'name'         => 'fiche_attachment_' . $i,
				'type'         => 'group',
				'sub_fields'   => array(
					array(
						'key'           => 'fiche_attachment_' . $i . '_title',
						'label'         => 'Titre du fichier',
						'name'          => 'fiche_attachment_title',
						'type'          => 'text',
						'column_width'  => '',
						'default_value' => '',
						'placeholder'   => '',
						'prepend'       => '',
						'append'        => '',
						'formatting'    => 'none',
						'maxlength'     => '',
					),
					array(
						'key'          => 'fiche_attachment_' . $i . '_file',
						'label'        => 'Fichier',
						'name'         => 'fiche_attachment_file',
						'type'         => 'file',
						'column_width' => '',
						'save_format'  => 'id',
						'library'      => 'all',
					),
				),
				'row_min'      => '',
				'row_limit'    => '',
				'layout'       => 'table',
				'button_label' => 'Ajouter un fichier joint',
			);
		}

		register_field_group( $fieldGroup );
	}

	/**
	 * @return void
	 */
	private static function addThemes() {
		register_taxonomy(
			'themes',
			array(
				'blog',
				'kit',
				'guide',
				'event',
				'incsub_wiki',
				'anim',
				'podcast',
			),
			array(
				'labels'            => array(
					'name'                       => _x( 'Thèmes', 'Taxonomy General Name', 'text_domain' ),
					'singular_name'              => _x( 'Thème', 'Taxonomy Singular Name', 'text_domain' ),
					'menu_name'                  => __( 'Thèmes', 'text_domain' ),
					'all_items'                  => __( 'Tous les thèmes', 'text_domain' ),
					'parent_item'                => __( 'Parent Item', 'text_domain' ),
					'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
					'new_item_name'              => __( 'Ajouter un thème', 'text_domain' ),
					'add_new_item'               => __( 'Ajouter un thème', 'text_domain' ),
					'edit_item'                  => __( 'Modifier le thème', 'text_domain' ),
					'update_item'                => __( 'Mettre à jour le thème', 'text_domain' ),
					'separate_items_with_commas' => __( 'Séparer par des virgules', 'text_domain' ),
					'search_items'               => __( 'Chercher un thème', 'text_domain' ),
					'add_or_remove_items'        => __( 'Ajouter ou supprimer des thèmes', 'text_domain' ),
					'choose_from_most_used'      => __( 'Choir parmi les thèmes les plus utilisés', 'text_domain' ),
					'not_found'                  => __( 'Aucun résultat', 'text_domain' ),
				),
				'hierarchical'      => true,
				'public'            => true,
				'show_ui'           => true,
				'show_in_rest'      => true,
				'show_admin_column' => true,
				'show_in_nav_menus' => true,
				'show_tagcloud'     => true,
				'rewrite'           => array(
					'slug'         => 'themes',
					'with_front'   => true,
					'hierarchical' => true,
				),
			)
		);
	}

	/**
	 * @return void
	 */
	private static function addActions() {
		register_taxonomy(
			'actions',
			array( 'incsub_wiki' ),
			array(
				'labels'            => array(
					'name'                       => _x( 'Actions', 'Taxonomy General Name', 'text_domain' ),
					'singular_name'              => _x( 'Action', 'Taxonomy Singular Name', 'text_domain' ),
					'menu_name'                  => __( 'Actions', 'text_domain' ),
					'all_items'                  => __( 'Tous les actions', 'text_domain' ),
					'parent_item'                => __( 'Parent Item', 'text_domain' ),
					'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
					'new_item_name'              => __( 'Ajouter une action', 'text_domain' ),
					'add_new_item'               => __( 'Ajouter une action', 'text_domain' ),
					'edit_item'                  => __( 'Modifier l\'action', 'text_domain' ),
					'update_item'                => __( 'Mettre à jour l\'action', 'text_domain' ),
					'separate_items_with_commas' => __( 'Séparer par des virgules', 'text_domain' ),
					'search_items'               => __( 'Chercher une action', 'text_domain' ),
					'add_or_remove_items'        => __( 'Ajouter ou supprimer des actions', 'text_domain' ),
					'choose_from_most_used'      => __( 'Choisir parmi les actions les plus utilisées', 'text_domain' ),
					'not_found'                  => __( 'Aucun résultat', 'text_domain' ),
				),
				'hierarchical'      => true,
				'public'            => true,
				'show_ui'           => true,
				'show_in_rest'      => true,
				'show_admin_column' => true,
				'show_in_nav_menus' => true,
				'show_tagcloud'     => true,
				'rewrite'           => array(
					'slug'         => 'actions',
					'with_front'   => true,
					'hierarchical' => true,
				),
			)
		);
	}

	/**
	 * @return void
	 */
	private static function addVideo() {
		register_post_type(
			'video',
			array(
				'labels'       => array(
					'name'          => 'Vidéos',
					'singular_name' => 'Vidéo',
					'add_new_item'  => 'Ajouter une vidéo',
				),
				'public'       => true,
				'has_archive'  => true,
				'show_in_menu' => 'animafac-tools',
				'menu_icon'    => 'dashicons-video-alt3',
				'supports'     => array( 'title', 'page-attributes' ),
				'taxonomies'   => array( 'themes' ),
			)
		);

		register_field_group(
			array(
				'title'    => 'Paramètres de la vidéo',
				'fields'   => array(
					array(
						'key'   => 'video_url',
						'label' => 'URL de la vidéo',
						'name'  => 'video_url',
						'type'  => 'oembed',
					),
				),
				'location' => array(
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'video',
						),
					),
				),
				'options'  => array(
					'position' => 'acf_after_title',
					'layout'   => 'no_box',
				),
			)
		);
	}

	/**
	 * @return void
	 */
	private static function addKit() {
		register_post_type(
			'kit',
			array(
				'label'               => __( 'Kits de campagne', 'text_domain' ),
				'description'         => __( 'Kits de campagne Description', 'text_domain' ),
				'labels'              => array(
					'name'               => _x( 'Kits de campagne', 'Post Type General Name', 'text_domain' ),
					'singular_name'      => _x( 'Kits de campagne', 'Post Type Singular Name', 'text_domain' ),
					'menu_name'          => __( 'Kits de campagne', 'text_domain' ),
					'parent_item_colon'  => __( 'Parent Item:', 'text_domain' ),
					'all_items'          => __( 'Kits de campagne', 'text_domain' ),
					'view_item'          => __( 'Voir le kit', 'text_domain' ),
					'add_new_item'       => __( 'Ajouter un kit', 'text_domain' ),
					'add_new'            => __( 'Ajouter un kit', 'text_domain' ),
					'edit_item'          => __( 'Modifier le kit', 'text_domain' ),
					'update_item'        => __( 'Mettre à jour le kit', 'text_domain' ),
					'search_items'       => __( 'Rechercher', 'text_domain' ),
					'not_found'          => __( 'Aucun résultat', 'text_domain' ),
					'not_found_in_trash' => __( 'Aucun résultat dans la corbeille', 'text_domain' ),
				),
				'supports'            => array(
					'title',
					'editor',
					'custom-fields',
					'revisions',
				),
				'taxonomies'          => array( 'taxo' ),
				'hierarchical'        => true,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => 'animafac-tools',
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 100,
				'menu_icon'           => '',
				'can_export'          => true,
				'has_archive'         => true,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'rewrite'             => array(
					'slug'       => 'kits-de-campagne',
					'with_front' => true,
					'pages'      => true,
					'feeds'      => true,
				),
				'capability_type'     => 'post',
			)
		);
	}

	/**
	 * @return void
	 */
	private static function addGlobalFields() {
		register_field_group(
			array(
				'id'         => 'acf_fichier-principal',
				'title'      => 'Fichier principal',
				'fields'     => array(
					array(
						'key'         => 'field_5357d858cdc13',
						'label'       => 'Fichier principal',
						'name'        => 'content_file',
						'type'        => 'file',
						'save_format' => 'id',
						'library'     => 'all',
					),
				),
				'location'   => array(
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'blog',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'kit',
							'order_no' => 0,
							'group_no' => 1,
						),
					),
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'anim',
							'order_no' => 0,
							'group_no' => 2,
						),
					),
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'guide',
							'order_no' => 0,
							'group_no' => 3,
						),
					),
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'event',
							'order_no' => 0,
							'group_no' => 4,
						),
					),
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'model',
							'order_no' => 0,
							'group_no' => 5,
						),
					),
				),
				'options'    => array(
					'position'       => 'normal',
					'layout'         => 'no_box',
					'hide_on_screen' => array(
						0 => 'excerpt',
						1 => 'custom_fields',
						2 => 'discussion',
						3 => 'comments',
						4 => 'revisions',
						5 => 'author',
						6 => 'format',
						7 => 'featured_image',
						8 => 'tags',
						9 => 'send-trackbacks',
					),
				),
				'menu_order' => -1,
			)
		);
		register_field_group(
			array(
				'id'         => 'acf_galerie-photo',
				'title'      => 'Galerie photo',
				'fields'     => array(
					array(
						'key'           => 'field_5357d70ffcc7a',
						'label'         => 'Galerie photos',
						'name'          => 'content_gallery',
						'type'          => 'relationship',
						'return_format' => 'id',
						'post_type'     => array(
							0 => 'attachment',
						),
						'filters'       => array(
							0 => 'search',
						),
						'elements'      => array( 'featured_image' ),
						'preview_size'  => 'thumbnail',
						'library'       => 'all',
					),
				),
				'location'   => array(
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'kit',
							'order_no' => 0,
							'group_no' => 1,
						),
					),
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'guide',
							'order_no' => 0,
							'group_no' => 3,
						),
					),
				),
				'options'    => array(
					'position'       => 'normal',
					'layout'         => 'no_box',
					'hide_on_screen' => array(
						0 => 'excerpt',
						1 => 'custom_fields',
						2 => 'discussion',
						3 => 'comments',
						4 => 'revisions',
						5 => 'author',
						6 => 'format',
						7 => 'featured_image',
						8 => 'tags',
						9 => 'send-trackbacks',
					),
				),
				'menu_order' => 2,
			)
		);
		register_field_group(
			array(
				'id'         => 'acf_illustration-principale',
				'title'      => 'Illustration principale',
				'fields'     => array(
					array(
						'key'          => 'field_5357d30c42957',
						'label'        => 'Illustration principale',
						'name'         => 'content_thumbnail',
						'type'         => 'image',
						'save_format'  => 'id',
						'preview_size' => 'thumbnail',
						'library'      => 'all',
					),
				),
				'location'   => array(
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'kit',
							'order_no' => 0,
							'group_no' => 1,
						),
					),
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'model',
							'order_no' => 0,
							'group_no' => 7,
						),
					),
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'podcast',
							'order_no' => 0,
							'group_no' => 8,
						),
					),
				),
				'options'    => array(
					'position'       => 'normal',
					'layout'         => 'no_box',
					'hide_on_screen' => array(
						0  => 'permalink',
						1  => 'excerpt',
						2  => 'custom_fields',
						3  => 'discussion',
						4  => 'comments',
						5  => 'revisions',
						6  => 'slug',
						7  => 'author',
						8  => 'format',
						9  => 'featured_image',
						10 => 'categories',
						11 => 'tags',
						12 => 'send-trackbacks',
					),
				),
				'menu_order' => 1,
			)
		);
		register_field_group(
			array(
				'id'         => 'acf_association-des-contenus',
				'title'      => 'Association des contenus',
				'fields'     => array(
					array(
						'key'             => 'field_530ef50d15ef1',
						'label'           => 'Associations de contenus',
						'name'            => 'content_featured',
						'type'            => 'relationship',
						'return_format'   => 'id',
						'post_type'       => array(
							0 => 'page',
							1 => 'blog',
							2 => 'kit',
							3 => 'anim',
							4 => 'guide',
							5 => 'event',
							6 => 'incsub_wiki',
						),
						'taxonomy'        => array(
							0 => 'all',
						),
						'filters'         => array(
							0 => 'search',
						),
						'result_elements' => array(
							0 => 'post_type',
							1 => 'post_title',
						),
						'max'             => 4,
					),
				),
				'location'   => array(
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'blog',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'kit',
							'order_no' => 0,
							'group_no' => 1,
						),
					),
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'event',
							'order_no' => 0,
							'group_no' => 3,
						),
					),
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'anim',
							'order_no' => 0,
							'group_no' => 4,
						),
					),
				),
				'options'    => array(
					'position'       => 'normal',
					'layout'         => 'no_box',
					'hide_on_screen' => array(
						0 => 'excerpt',
						1 => 'custom_fields',
						2 => 'discussion',
						3 => 'slug',
						4 => 'author',
						5 => 'featured_image',
						6 => 'tags',
						7 => 'send-trackbacks',
					),
				),
				'menu_order' => 4,
			)
		);
	}

	/**
	 * @return void
	 */
	private static function addGuide() {
		register_post_type(
			'guide',
			array(
				'label'               => __( 'Guides pratiques', 'text_domain' ),
				'description'         => __( 'Guides pratiques Description', 'text_domain' ),
				'labels'              => array(
					'name'               => _x( 'Guides pratiques', 'Post Type General Name', 'text_domain' ),
					'singular_name'      => _x( 'Guide pratique', 'Post Type Singular Name', 'text_domain' ),
					'menu_name'          => __( 'Guides pratiques', 'text_domain' ),
					'parent_item_colon'  => __( 'Parent Item:', 'text_domain' ),
					'all_items'          => __( 'Guides pratiques', 'text_domain' ),
					'view_item'          => __( 'Voir le guide', 'text_domain' ),
					'add_new_item'       => __( 'Ajouter un guide', 'text_domain' ),
					'add_new'            => __( 'Ajouter un guide', 'text_domain' ),
					'edit_item'          => __( 'Modifier le guide' ),
					'update_item'        => __( 'Mettre à jour le guide', 'text_domain' ),
					'search_items'       => __( 'Rechercher', 'text_domain' ),
					'not_found'          => __( 'Aucun résultat', 'text_domain' ),
					'not_found_in_trash' => __( 'Aucun résultat dans la corbeille', 'text_domain' ),
				),
				'supports'            => array(
					'title',
					'editor',
					'custom-fields',
					'author',
					'page-attributes',
					'revisions',
				),
				'hierarchical'        => true,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => 'animafac-tools',
				'show_in_nav_menus'   => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => 100,
				'menu_icon'           => '',
				'can_export'          => true,
				'has_archive'         => true,
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'rewrite'             => array(
					'slug'       => 'guides-pratiques',
					'with_front' => true,
					'pages'      => true,
					'feeds'      => true,
				),
				'capability_type'     => 'page',
			)
		);
	}

	/**
	 * @return void
	 */
	private static function addModel() {
		register_post_type(
			'model',
			array(
				'labels'       => array(
					'name'          => 'Modèles de documents',
					'singular_name' => 'Modèle de document',
					'add_new_item'  => 'Ajouter un modèle',
				),
				'public'       => true,
				'has_archive'  => true,
				'show_in_menu' => 'animafac-tools',
				'menu_icon'    => 'dashicons-media-document',
				'supports'     => array( 'title', 'editor', 'thumbnail' ),
				'taxonomies'   => array( 'themes' ),
			)
		);
	}

	/**
	 * @return void
	 */
	public static function init() {
		// Taxonomies
		self::addThemes();
		self::addActions();

		// Types de contenu
		self::addWiki();
		self::addVideo();
		self::addKit();
		self::addGuide();
		self::addModel();
		self::addPodcast();

		// Champs présents sur plusieurs types de contenu
		self::addGlobalFields();
	}

	/**
	 * @return void
	 */
	private static function addPodcast(): void {
		register_post_type(
			'podcast',
			array(
				'labels'       => array(
					'name'          => 'Podcasts',
					'singular_name' => 'Podcast',
					'add_new_item'  => 'Ajouter un podcast',
				),
				'public'       => true,
				'has_archive'  => true,
				'show_in_menu' => 'animafac-tools',
				'supports'     => array( 'title', 'editor' ),
			)
		);

		register_field_group(
			array(
				'title'    => 'Paramètres du podcast',
				'fields'   => array(
					array(
						'key'   => 'podcast_url',
						'label' => 'URL du podcast',
						'name'  => 'podcast_url',
						'type'  => 'oembed',
					),
				),
				'location' => array(
					array(
						array(
							'param'    => 'post_type',
							'operator' => '==',
							'value'    => 'podcast',
						),
					),
				),
				'options'  => array(
					'layout' => 'no_box',
				),
			)
		);
	}

	/**
	 * @return void
	 */
	public static function adminMenu() {
		add_menu_page(
			'Boîte à outils',
			'Boîte à outils',
			'edit_posts',
			'animafac-tools',
			null,
			'dashicons-admin-tools',
			20
		);
		add_submenu_page(
			'animafac-tools',
			'Thèmes',
			'Thèmes',
			'edit_posts',
			'edit-tags.php?taxonomy=themes'
		);
	}

	/**
	 * @param string $file
	 *
	 * @return string
	 */
	public static function parent_file( string $file ): string {
		$screen = get_current_screen();
		if ( isset( $screen ) && 'edit-themes' === $screen->id ) {
			$file = 'animafac-tools';
		}

		return $file;
	}

	/**
	 * @param $atts
	 * @param $content
	 *
	 * @return string
	 */
	public static function toggle( $atts, $content ): string {
		$return = '';
		if ( isset( $atts['title'] ) ) {
			$return .= '<h1>' . $atts['title'] . '</h1>';
		}
		$return .= do_shortcode( $content );

		return $return;
	}

	/**
	 * @param $atts
	 * @param $content
	 *
	 * @return string
	 * @noinspection PhpUnusedParameterInspection
	 */
	public static function toggles( $atts, $content ): string {
		$markupFixer = new MarkupFixer();

		return $markupFixer->fix( do_shortcode( $content ), 1, 1 );
	}

	/**
	 * @return void
	 */
	public static function wpHead() {
		if ( is_singular( 'incsub_wiki' ) ) {
			add_shortcode( 'toggle', array( self::class, 'toggle' ) );
			add_shortcode( 'toggles', array( self::class, 'toggles' ) );
		}
	}

	/**
	 * @return void
	 */
	public static function widgets_init(): void {
		register_widget( TOC::class );
		register_widget( About::class );
		register_widget( Files::class );
		register_widget( License::class );
		register_widget( Related::class );
	}
}
