<?php

namespace AnimafacToolbox\Widgets;

use WP_Widget;

class About extends WP_Widget {

	function __construct() {
		parent::__construct(
			'animafac-toolbox-about',
			__( 'À propos', 'animafac-plateforme' )
		);
	}

	/**
	 * @param array $args
	 * @param array $instance
	 *
	 * @return void
	 */
	function widget( $args, $instance ): void {
		$terms    = get_the_terms( get_the_ID(), 'themes' );
		$category = $terms[0];

		if ( is_singular( 'guide' ) ) {
			$published_text = __( 'Guide publié' );
		} elseif ( is_singular( 'incsub_wiki' ) ) {
			$published_text = __( 'Fiche pratique publiée' );
		} elseif ( is_singular( 'kit' ) ) {
			$published_text = __( 'Kit publié' );
		} elseif ( is_singular( 'podcast' ) ) {
			$published_text = __( 'Podcast publié' );
		} else {
			$published_text = __( 'Page publiée' );
		}

		// Titre
		echo $args['before_widget'];
		echo $args['before_title'] . $this->name . $args['after_title'];

		// Contenu
		echo sprintf(
			// translators: 1 = content type, 2 = date, 3 = term link
			__( '%1$s le %2$s dans la thématique %3$s', 'animafac-plateforme' ),
			$published_text,
			esc_html( get_the_date() ),
			'<a href="' . esc_url( get_category_link( $category ) ) . '">' . esc_html( $category->name ) . '</a>'
		);
		echo $args['after_widget'];
	}

}
