<?php

namespace AnimafacToolbox\Widgets;

use DateTime;
use WP_Widget;

class Files extends WP_Widget {

	function __construct() {
		parent::__construct(
			'animafac-toolbox-files',
			__( 'Documents à télécharger', 'animafac-plateforme' )
		);
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	private function getFiles(): array {
		$files        = array();
		$content_file = get_field( 'content_file' );
		if ( $content_file && get_post( $content_file ) ) {
			$files[] = array(
				'fiche_attachment_title' => get_the_title( $content_file ),
				'fiche_attachment_file'  => $content_file,
			);
		}

		for ( $i = 0; $i <= 5; $i ++ ) {
			$attachment = get_field( 'fiche_attachment_' . $i );
			if ( isset( $attachment['fiche_attachment_file'] )
				&& $attachment['fiche_attachment_file']
				&& get_post( $attachment['fiche_attachment_file'] )
			) {
				$files[] = $attachment;
			}
		}

		return $files;
	}

	/**
	 * @param array $args
	 * @param array $instance
	 *
	 * @return void
	 * @throws \Exception
	 */
	function widget( $args, $instance ): void {
		$files = $this->getFiles();
		if ( ! empty( $files ) ) {
			// Titre
			echo $args['before_widget'];
			echo $args['before_title'] . $this->name . $args['after_title'];

			// Contenu
			echo '<ul>';
			foreach ( $files as $file ) {
				echo '<li>';
				echo '<a href="' . esc_url( wp_get_attachment_url( $file['fiche_attachment_file'] ) ) . '" target="_blank">';
				echo esc_html( $file['fiche_attachment_title'] );
				echo '</a>';
				echo '</li>';
			}
			echo '</ul>' . $args['after_widget'];
		}
	}

}
