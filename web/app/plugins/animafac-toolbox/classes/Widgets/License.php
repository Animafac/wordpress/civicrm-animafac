<?php

namespace AnimafacToolbox\Widgets;

use DateTime;
use WP_Widget;

class License extends WP_Widget {

	function __construct() {
		parent::__construct(
			'animafac-toolbox-license',
			__( 'Licence du contenu', 'animafac-plateforme' )
		);
	}

	/**
	 * @param array $args
	 * @param array $instance
	 *
	 * @return void
	 * @throws \Exception
	 */
	function widget( $args, $instance ): void {
		if ( is_singular( 'guide' ) ) {
			$content_type = __( 'ce guide', 'animafac-plateforme' );
		} elseif ( is_singular( 'incsub_wiki' ) ) {
			$content_type = __( 'cette fiche', 'animafac-plateforme' );
		} else {
			$content_type = __( 'cette page', 'animafac-plateforme' );
		}

		// Titre
		echo $args['before_widget'];
		echo $args['before_title'] . $this->name . $args['after_title'];

		// Contenu
		echo sprintf(
			// translators: 1 = content type, 2 = license link
			__(
				'Sauf mention contraire, le texte de %1$s est mis à disposition
		        selon les termes de la %2$s.
		        Les images restent la propriété de leurs auteur.e.s respectif.ve.s.',
				'animafac-plateforme'
			),
			$content_type,
			'<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr" target="_blank">' .
				__( 'Licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International', 'animafac-plateforme' ) .
				'</a>'
		);
		echo $args['after_widget'];
	}

}
