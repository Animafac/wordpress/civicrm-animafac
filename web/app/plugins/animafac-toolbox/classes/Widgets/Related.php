<?php

namespace AnimafacToolbox\Widgets;

use DateTime;
use WP_Query;
use WP_Widget;

class Related extends WP_Widget {

	function __construct() {
		parent::__construct(
			'animafac-toolbox-related',
			__( 'Ressources associées', 'animafac-plateforme' )
		);
	}

	/**
	 * @return array
	 */
	private function getPosts(): array {
		$terms = array();
		foreach ( get_the_terms( get_the_ID(), 'themes' ) as $term ) {
			$terms[] = $term->slug;
		}
		$tax_query = array(
			array(
				'taxonomy' => 'themes',
				'field'    => 'slug',
				'terms'    => $terms,
			),
		);
		if ( is_singular( 'guide' ) ) {
			$same_type = 'incsub_wiki';
		} else {
			$same_type = get_post_type();
		}

		if ( ! empty( $tax_query ) ) {
			$same_type_query = new WP_Query(
				array(
					'post_type'      => $same_type,
					'posts_per_page' => 3,
					'post__not_in'   => array( get_the_ID() ),
					'tax_query'      => $tax_query,
				)
			);
			$guide_query     = new WP_Query(
				array(
					'post_type'      => 'guide',
					'posts_per_page' => 1,
					'post__not_in'   => array( get_the_ID() ),
					'tax_query'      => $tax_query,
				)
			);

			return array_merge( $guide_query->get_posts(), $same_type_query->get_posts() );
		}

		return array();
	}

	/**
	 * @param array $args
	 * @param array $instance
	 *
	 * @return void
	 * @throws \Exception
	 */
	function widget( $args, $instance ): void {
		// Titre
		echo $args['before_widget'];
		echo $args['before_title'] . $this->name . $args['after_title'];

		// Contenu
		echo '<ul>';

		/** @var \WP_Post $post */
		foreach ( $this->getPosts() as $post ) {
			$theme = current( get_the_terms( $post, 'themes' ) );

			echo '<li>';
			if ( $theme ) {
				echo '<span class="theme">' . esc_html( $theme->name ) . '</span>';
			}
			echo '<a href="' . esc_url( get_the_permalink( $post ) ) . '">' . esc_html( get_the_title( $post ) ) . '</a>';
			echo '<div class="excerpt">' . esc_html( get_the_excerpt( $post ) ) . '</div>';
			echo '</li>';
		}
		echo '</ul>' . $args['after_widget'];
	}

}
