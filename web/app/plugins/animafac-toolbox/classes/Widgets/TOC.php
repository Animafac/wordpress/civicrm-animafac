<?php

namespace AnimafacToolbox\Widgets;

use TOC\TocGenerator;
use WP_Widget;

class TOC extends WP_Widget {

	function __construct() {
		parent::__construct(
			'animafac-toolbox-toc',
			__( 'Sommaire', 'animafac-plateforme' )
		);
	}

	/**
	 * @param array $args
	 * @param array $instance
	 *
	 * @return void
	 */
	function widget( $args, $instance ): void {
		$toc_generator = new TocGenerator();
		$content       = do_shortcode( wp_kses_post( get_the_content() ) );

		if ( ! empty( $content ) ) {
			$toc = $toc_generator->getHtmlMenu( $content, 1, 1 );

			if ( ! empty( $toc ) ) {
				echo $args['before_widget'];
				echo $args['before_title'] . $this->name . $args['after_title'];
				echo wp_kses_post( $toc );
				echo $args['after_widget'];
			}
		}
	}

}
